package com.digifreneur.gda.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.digifreneur.gda.model.audit.DateAudit;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.annotations.NaturalId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@Table(name = "users", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"email"}),
    @UniqueConstraint(columnNames = {"phone"})
})
public class User extends DateAudit {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @NaturalId
    @Size(max = 40)
    @Column(name = "email", nullable = false, length = 100)
    @Email
    private String email;

    @NotBlank
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Size(max = 100)
    @Column(name = "password", nullable = false, length = 100)
    private String password;

    @NotBlank
    @Column(name = "first_name", nullable = false, length = 100)
    @Size(max = 40)
    private String firstName;

    @Column(name = "last_name", nullable = true, length = 100)
    @Size(max = 40)
    private String lastName;

    @NotBlank
    @NaturalId
    @Column(name = "phone", nullable = false, length = 36)
    private String phone;

    @NotBlank
    @Column(name = "gender", nullable = false, length = 2)
    @Size(max = 2)
    private String gender;

    @NotBlank
    @Column(name = "dob", columnDefinition = "DATE", nullable = false)
    // @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dob;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_role", 
        joinColumns = @JoinColumn(
            name = "user_id",
            referencedColumnName = "id"
        ),
        inverseJoinColumns = @JoinColumn(
            name = "role_id",
            referencedColumnName = "id"
        )
    )
    private List<Role> roles;

    public User(String email, String password, String firstName, String lastName, String phone, String gender, LocalDate dob) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.gender = gender;
        this.dob = dob;
    }

    public List<Role> getRoles() {
        return roles == null ? null : new ArrayList<>(roles);
    }

    public void setRoles(List<Role> roles) {
        if (roles == null) {
            this.roles = null;
        } else {
            this.roles = Collections.unmodifiableList(roles);
        }
    }
}
