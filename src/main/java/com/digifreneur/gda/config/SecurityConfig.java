package com.digifreneur.gda.config;

import com.digifreneur.gda.security.JwtAuthenticationEntryPoint;
import com.digifreneur.gda.security.JwtAuthenticationFilter;
import com.digifreneur.gda.service.CustomUserDetailsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableWebSecurity(debug = true)
@EnableGlobalMethodSecurity(
    securedEnabled = true, 
    jsr250Enabled = true, 
    prePostEnabled = true
)
@EnableJpaRepositories(basePackages = "com.digifreneur.gda.repository")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    private CustomUserDetailsService customUserDetailsService;
    private JwtAuthenticationEntryPoint jwtEntryPoint;
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @Autowired
    public SecurityConfig(CustomUserDetailsService customUserDetailsService,
            JwtAuthenticationEntryPoint jwtEntryPoint, JwtAuthenticationFilter jwtAuthenticationFilter) {
        this.customUserDetailsService = customUserDetailsService;
        this.jwtEntryPoint = jwtEntryPoint;
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String[] publicAccess = { 
                "/api/v1/product/**", 
                "/api/v1/category/**", 
                "/api/v1/brand/**", 
                "/api/v1/store/**", 
        };
        String[] adminAccess = { "/api/v1/admin/**" };
        String[] userAccess = { "/api/v1/user/**" };

        http.cors().and().csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(jwtEntryPoint)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(
                    HttpMethod.GET, publicAccess).permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/auth/**").permitAll()
                .antMatchers(HttpMethod.GET, 
                    "/api/v1/user/checkPhoneAvailability", 
                    "/api/v1/user/checkEmailAvailability").permitAll()
                .antMatchers(adminAccess).hasRole("ADMIN")
                .antMatchers(userAccess).access("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
                .anyRequest()
                .authenticated()
                ;

        http.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources/**",
                "/configuration/security", "/swagger-ui.html", "/webjars/**");
    }

    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
