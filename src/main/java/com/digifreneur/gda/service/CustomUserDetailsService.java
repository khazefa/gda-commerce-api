package com.digifreneur.gda.service;

import com.digifreneur.gda.model.User;
import com.digifreneur.gda.repository.UserRepository;
import com.digifreneur.gda.security.UserPrincipal;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {
        boolean isEmail = EmailValidator.getInstance().isValid(username);
        String email = null, phone = null;
        if (isEmail) {
            email = username;
        } else {
            phone = username;
        }

        User user = userRepository.findByEmailOrPhone(
                email, phone)
                .orElseThrow(() -> new UsernameNotFoundException(
                        String.format("User not found with this email or phone: %s", username)));
        return UserPrincipal.create(user);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User not found with id: %s", id)));

        return UserPrincipal.create(user);
    }
}
