package com.digifreneur.gda.repository;

import java.util.Optional;

import com.digifreneur.gda.model.Role;
import com.digifreneur.gda.model.RoleName;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(String roleUser);
    
    Optional<Role> findByName(RoleName roleUser);

}
