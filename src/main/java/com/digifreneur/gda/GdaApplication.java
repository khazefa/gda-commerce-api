package com.digifreneur.gda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.convert.Jsr310Converters;

// @SuppressWarnings("deprecation")
// @SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@SpringBootApplication
@EntityScan(basePackageClasses = { GdaApplication.class, Jsr310Converters.class })
public class GdaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GdaApplication.class, args);
	}
}
