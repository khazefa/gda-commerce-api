package com.digifreneur.gda.controller.v1;

import java.util.List;

import com.digifreneur.gda.model.Product;
import com.digifreneur.gda.model.payload.ProductDetailResponse;
import com.digifreneur.gda.service.ProductService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    
    private final Logger log = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts() {
        log.debug("REST request to get Products");
        List<Product> products = productService.getAllProducts();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable(name = "id") Long id) {
        log.debug("REST Request to get Product : {}", id);
        Product product = productService.getProduct(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }
    
    @GetMapping("/detail-info/{id}")
    public ResponseEntity<ProductDetailResponse> getProductInfo(@PathVariable(name = "id") Long id) {
        log.debug("REST Request to get Product Detail Information: {}", id);
        ProductDetailResponse product = productService.getProductDetailInfo(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }
}
